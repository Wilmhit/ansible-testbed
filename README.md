# ansible-testbed

Setup ansible testbed in seconds!

### Usage

Use this as your inventory:

```yaml
virtualmachines:
  hosts:
    main_server:
      ansible_host: ubuntu
      ansible_connection: docker
      ansible_user: ubuntu
```

(adjust as needed)

Next, run your ansible playbooks on `virtualmachines` host group.

### Repository

Main repository for this dockerfile is located at
[gitlab.com/Wilmhit/ansible-testbed](https://gitlab.com/Wilmhit/ansible-testbed)

*Always look at the source if you're running something found on the internet*
